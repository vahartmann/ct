/***********************************************************************************
Copyright (c) 2017, Michael Neunert, Markus Giftthaler, Markus Stäuble, Diego Pardo,
Farbod Farshidian. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ETH ZURICH nor the names of its contributors may be used
      to endorse or promote products derived from this software without specific
      prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL ETH ZURICH BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************************/


#ifndef INCLUDE_CT_CORE_TYPES_H_
#define INCLUDE_CT_CORE_TYPES_H_

#include "types/Time.h"

#include "types/AutoDiff.h"
#include "types/ControlVector.h"
#include "types/StateVector.h"
#include "types/FeedbackMatrix.h"
#include "types/StateMatrix.h"
#include "types/ControlMatrix.h"
#include "types/StateControlMatrix.h"

#include "types/trajectories/ScalarArray.h"
#include "types/trajectories/TimeArray.h"
#include "types/trajectories/StateVectorArray.h"
#include "types/trajectories/StateTrajectory.h"
#include "types/trajectories/ControlTrajectory.h"
#include "types/trajectories/ControlVectorArray.h"
#include "types/trajectories/FeedbackArray.h"
#include "types/trajectories/StateMatrixArray.h"
#include "types/trajectories/ControlMatrixArray.h"
#include "types/trajectories/ControlMatrixTrajectory.h"
#include "types/trajectories/StateControlMatrixArray.h"
#include "types/trajectories/FeedbackArray.h"
#include "types/trajectories/DiscreteTrajectoryBase.h"


#endif /* INCLUDE_CT_CORE_TYPES_H_ */
